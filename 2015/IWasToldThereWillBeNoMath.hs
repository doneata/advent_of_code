
import qualified Text.Parsec as Parsec

data Cuboid = Cuboid {
        length :: Int,
        width  :: Int,
        height :: Int
    } deriving Show

parseCuboid :: Parsec.Parsec String () Cuboid
parseCuboid = do
    length <- Parsec.many1 Parsec.digit
    Parsec.char 'x'
    width <- Parsec.many1 Parsec.digit
    Parsec.char 'x'
    height <- Parsec.many1 Parsec.digit
    return $ Cuboid (read length :: Int)
                    (read width  :: Int)
                    (read height :: Int)

area :: Cuboid -> Int
area (Cuboid l w h) = 2 * (l * w + w * h + h * l)

cube :: Cuboid -> Int
cube (Cuboid l w h) = l * w * h

smallestArea :: Cuboid -> Int
smallestArea (Cuboid l w h) = minimum [l * w, w * h, h * l]

smallestPerimeter :: Cuboid -> Int
smallestPerimeter (Cuboid l w h) = 2 * minimum [l + w, w + h, h + l]

lineToPaper :: String -> Int
lineToPaper s = case Parsec.parse parseCuboid "" s of
        Left  _      -> 0
        Right cuboid -> area cuboid + smallestArea cuboid

lineToRibbon :: String -> Int
lineToRibbon s = case Parsec.parse parseCuboid "" s of
        Left  _      -> 0
        Right cuboid -> smallestPerimeter cuboid + cube cuboid

main :: IO()
main = 
    let
        total f ss = sum . map f . lines $ ss
    in
        do
        ss <- readFile "data/02.txt"
        putStrLn $ "Total square feet of wrapping paper: " ++ (show $ total lineToPaper ss)
        putStrLn $ "Total feet of ribbon: " ++ (show $ total lineToRibbon ss)

