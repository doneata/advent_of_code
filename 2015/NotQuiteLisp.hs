
type Floor = Int

takeWhileInclusive :: (a -> Bool) -> [a] -> [a]
takeWhileInclusive _ [] = []
takeWhileInclusive p (x:xs) = x : if p x then takeWhileInclusive p xs
                                         else []

step :: (String, Floor) -> (String, Floor)
step (('(':xs), n) = (xs, (n + 1))
step ((')':xs), n) = (xs, (n - 1))

count :: String -> Floor
count xs = snd $ last $ takeWhileInclusive ((/= "\n") . fst) (iterate step (xs, 0))

basement :: String -> Int
basement xs = snd $ head $ filter ((== -1) . snd . fst) $ zip (iterate step (xs, 0)) [0..]

main :: IO()
main = do
    s <- readFile "data/01.txt"
    putStrLn $ "Santa is at floor " ++ (show $ count s)
    putStrLn $ "Santa entered the basement at step " ++ (show $ basement s)

