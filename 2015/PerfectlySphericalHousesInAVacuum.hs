
import Data.List (nub)

data Pos = Pos { lat :: Int, lon :: Int } deriving (Show, Eq)

move :: Pos -> Char -> Pos
move (Pos lat lon) '^' = Pos (lat + 1) lon
move (Pos lat lon) 'v' = Pos (lat - 1) lon
move (Pos lat lon) '<' = Pos lat (lon + 1)
move (Pos lat lon) '>' = Pos lat (lon - 1)
move pos           _   = pos

getMoves :: String -> [Pos]
getMoves = scanl move (Pos 0 0)

count :: String -> Int
count = length . nub . getMoves

split :: [a] -> ([a], [a])
split []       = ([], [])
split [x]      = ([x], [])
split (x:y:xs) = (x:xp, y:yp) where (xp, yp) = split xs

count2 :: String -> Int
count2 ss = length $ nub $ getMoves ss1 ++ getMoves ss2
    where
        (ss1, ss2) = split ss

main :: IO ()
main = do
    ss <- readFile "data/03.txt"
    putStrLn $ "Number of houses that received at least one present from Santa: " ++ (show $ count ss)
    putStrLn $ "Number of houses that received at least one present from Santa or RoboSanta: " ++ (show $ count2 ss)

