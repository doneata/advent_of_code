
import Crypto.Hash.MD5 (hash)
import Data.ByteString.Base16 (encode)
import Data.ByteString.Char8 (pack)
import Data.ByteString (ByteString)
import qualified Data.ByteString as B

p = "bgvyzdsv"

toByteString :: String -> Int -> ByteString
toByteString s i = pack $ s ++ show i

isKey :: ByteString -> Bool
isKey s = B.take 5 s == pack "00000"

main :: IO ()
main = do
    putStrLn $ "Found answer: " ++ show $ head $ filter (isKey . encode . hash . toByteString p) [1..]

