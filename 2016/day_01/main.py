import math
import pdb

from collections import namedtuple


Vector = namedtuple('Vector', 'x y')


ANGLES = {
    'R': - math.pi / 2,
    'L': + math.pi / 2,
}


def load_data_test():
    for datum in 'R8, R4, R4, R8'.split(', '):
        direction, *step = datum
        yield direction, int(''.join(step))


def load_data():
    with open('input.txt', 'r') as f:
        for datum in f.read().split(', '):
            direction, *step = datum
            yield direction, int(''.join(step))


def rotate(vect, angle):
    return Vector(
        x=int(vect.x * math.cos(angle) - vect.y * math.sin(angle)),
        y=int(vect.x * math.sin(angle) + vect.y * math.cos(angle)),
    )


def move(position, vector, step):
    return Vector(
        x=position.x + step * vector.x,
        y=position.y + step * vector.y,
    )


def part_one():
    vect = Vector(x=0, y=1)
    pos = Vector(x=0, y=0)
    for i, (d, s) in enumerate(load_data()):
        vect = rotate(vect, ANGLES[d])
        pos = move(pos, vect, s)
        print(i, d, s, pos)
    return pos


def part_two():

    vect = Vector(x=0, y=1)
    pos = Vector(x=0, y=0)

    visited = set([pos])

    for i, (d, s) in enumerate(load_data()):

        vect = rotate(vect, ANGLES[d])

        for _ in range(s):

            pos = move(pos, vect, 1)

            print(i, d, s, pos)

            if pos in visited:
                return pos
            else:
                visited.add(pos)



def main():
    pos = part_two()
    print(abs(pos.x) + abs(pos.y))
    

if __name__ == '__main__':
    main()
