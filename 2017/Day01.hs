import Data.Char (digitToInt, isSpace)

f :: String -> Int -> Int
f s n = sum 
      $ map (digitToInt . fst)
      $ filter (\ (x, y) -> x == y)
      $ zip s (iterate tail (cycle s) !! n)

rstrip :: String -> String
rstrip = reverse . dropWhile isSpace . reverse

main :: IO()
main = do
    t <- readFile "Day01.txt"
    let s = rstrip t
    putStrLn $ show $ f s 1
    putStrLn $ show $ f s $ length s `div` 2
