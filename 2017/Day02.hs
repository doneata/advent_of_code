import Data.Char (isSpace)
import Data.List.Split (splitOn)

-- Part 1
f :: [[Int]] -> Int
f = sum . map (\ xs -> (maximum xs) - (minimum xs))

-- Part 2
g :: [[Int]] -> Int
g xxs = sum $ map ((uncurry div) . head . areDiv) xxs

areDiv :: Integral a => [a] -> [(a, a)]
areDiv xs = [(x,y) | x <- xs, y <- xs, x /= y, x `mod` y == 0]

rstrip :: String -> String
rstrip = reverse . dropWhile isSpace . reverse

p :: String -> [[Int]]
p = map (map read) . map (splitOn " ") . splitOn "\n" . rstrip

main :: IO()
main = do
    s <- readFile "Day02.txt"
    let xss = p s
    putStrLn $ show $ f xss
    putStrLn $ show $ g xss
