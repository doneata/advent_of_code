data State = State
        { size :: Int
        , pos :: (Int, Int)
        , dir :: (Int, Int) }
    deriving Show

initState :: State
initState = State 0 (0, 0) (1, 0)

-- Part 1
toUpdate :: State -> Bool
toUpdate state = if dir state == (1, 0)
                 then x == s + 1 && y == -s
                 else abs x == s && abs y == s
    where
        (x, y) = pos state
        s      = size state

rotateCounterClockwise :: (Int, Int) -> (Int, Int)
rotateCounterClockwise ( 1,  0) = ( 0,  1)
rotateCounterClockwise ( 0,  1) = (-1,  0)
rotateCounterClockwise (-1,  0) = ( 0, -1)
rotateCounterClockwise ( 0, -1) = ( 1,  0)

updateDir :: State -> (Int, Int)
updateDir state | toUpdate state = rotateCounterClockwise d
                | otherwise      = d
    where
        d = dir state

move :: State -> State
move state@(State s (x, y) (dx, dy)) = State s' (x', y') (dx', dy')
    where
        (x', y')   = (x + dx, y + dy)
        (dx', dy') = updateDir (State s (x', y') (dx, dy))
        s'         = maximum [s, abs x', abs y']

f :: Int -> Int
f n = abs x + abs y
    where
        states = iterate move initState
        (x, y) = pos (states !! (n - 1))

-- Part 2

main :: IO()
main = do
    let count = 265149
    putStrLn $ show $ f count
