import Data.Char (isSpace)
import Data.List.Split (splitOn)
import Data.List (nub, sort)

isValid :: [String] -> Bool
isValid xs = length xs == length (nub xs)

-- Part 1
f :: [[String]] -> Int
f = length . filter isValid

-- Part 2
g :: [[String]] -> Int
g = length . filter isValid . map (map sort)

rstrip :: String -> String
rstrip = reverse . dropWhile isSpace . reverse

p :: String -> [[String]]
p = map (splitOn " ") . splitOn "\n" . rstrip

main :: IO()
main = do
    s <- readFile "Day04.txt"
    let xss = p s
    putStrLn $ show $ f xss
    putStrLn $ show $ g xss
