import Data.Char (isSpace)
import Data.List (findIndex)
import Data.List.Split (splitOn)
import Data.Maybe (isNothing)
import Data.Vector ((!?), (//), fromList, Vector)
import Debug.Trace (trace)

type State = (Int, Vector Int)

next :: (Int -> Int) -> State -> Maybe State
next f (i, v) =
    case (v !? i) of
        Nothing -> Nothing
        Just x  -> Just (i + x, v // [(i, f x)])

iter :: Maybe State -> (Int -> Int) -> Int -> Int
iter Nothing  f i = i - 1
iter (Just s) f i = iter (next f s) f (i + 1)

-- Utils
rstrip :: String -> String
rstrip = reverse . dropWhile isSpace . reverse

processFile :: String -> [Int]
processFile = map read . splitOn "\n" . rstrip

g :: Int -> Int
g x | x >= 3    = x - 1
    | otherwise = x + 1

main :: IO()
main = do
    s <- readFile "Day05.txt"
    let st = Just (0, fromList (processFile s))
    -- Part 1
    putStrLn $ show $ iter st (+1) 0
    -- Part 2
    putStrLn $ show $ iter st g 0
