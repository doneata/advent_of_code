from itertools import cycle

with open("Day01.txt", 'r') as f:
    text = f.read().strip()

def cycle_n(xs, n):
    ys = cycle(xs)
    for _ in range(n):
        next(ys)
    return ys

def f(text, n):
    return sum(int(x) for x, y in zip(text, cycle_n(text, n)) if x == y)

print(f(text, 1))
print(f(text, len(text) / 2))
